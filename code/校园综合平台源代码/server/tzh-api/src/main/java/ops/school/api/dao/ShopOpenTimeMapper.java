package ops.school.api.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import ops.school.api.entity.ShopOpenTime;

public interface ShopOpenTimeMapper extends BaseMapper<ShopOpenTime> {
}