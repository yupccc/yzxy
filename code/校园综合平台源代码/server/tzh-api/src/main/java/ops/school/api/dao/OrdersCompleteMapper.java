package ops.school.api.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import ops.school.api.entity.OrdersComplete;

public interface OrdersCompleteMapper extends BaseMapper<OrdersComplete> {
}