package ops.school.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import ops.school.api.entity.Orders;
import ops.school.api.enums.PublicErrorEnums;
import ops.school.api.exception.Assertions;
import ops.school.api.service.*;
import ops.school.api.util.RedisUtil;
import ops.school.api.util.ResponseObject;
import ops.school.api.util.SpringUtil;
import ops.school.api.util.Util;
import ops.school.service.TOrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@RestController
@Api(tags = "订单模块")
@RequestMapping("ops/orders")
public class OrdersController {

    @Autowired
    private OrdersService ordersService;
    @Autowired
    private TOrdersService tOrdersService;
    @Autowired
    private SchoolService schoolService;
    @Autowired
    private ProductService productService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private WxUserService wxUserService;
    @Autowired
    private OrderProductService orderProductService;

    @Autowired
    private RedisUtil redisUtil;



    @ApiOperation(value = "查询", httpMethod = "POST")
    @PostMapping("find")
    public ResponseObject find(HttpServletRequest request, HttpServletResponse response,
                               Orders orders) {
        return new ResponseObject(false, "请联系qq471594060");
    }


    /**
     * @param request
     * @param response
     * @param buildId
     * @param beginTime
     * @param endTime
     * @date: 2019/7/15 18:12
     * @author: QinDaoFang
     * @version:version
     * @return: ops.school.api.util.ResponseObject
     * @Desc: desc
     */
    @ApiOperation(value = "根据楼栋和时间范围查询订单等信息", httpMethod = "POST")
    @PostMapping("findOrdersByFloor")
    public ResponseObject countKindsOrderByBIdAndTime(HttpServletRequest request, HttpServletResponse response, @RequestParam Integer buildId, @RequestParam String beginTime, @RequestParam String endTime) {
        return new ResponseObject(false, "请联系qq471594060");
    }


    /////////////////////////////////////////////////////////////android/////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////android/////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////android/////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////android/////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////android/////////////////////////////////////////////////////////////////

    @ApiOperation(value = "商家查询待接手订单", httpMethod = "POST")
    @PostMapping("android/findDjs")
    public ResponseObject android_findDjs(HttpServletRequest request, HttpServletResponse response, int shopId) {
        return new ResponseObject(false, "请联系qq471594060");
    }

    @ApiOperation(value = "商家查询订单", httpMethod = "POST")
    @PostMapping("android/findorders")
    public ResponseObject android_findorders(HttpServletRequest request, HttpServletResponse response, int shopId, int page, int size) {
        return new ResponseObject(false, "请联系qq471594060");
    }

    @ApiOperation(value = "商家查询已接手订单", httpMethod = "POST")
    @PostMapping("android/findordersyjs")
    public ResponseObject android_findorders2(HttpServletRequest request, HttpServletResponse response, int shopId, int page, int size) {
        return new ResponseObject(false, "请联系qq471594060");
    }

    @ApiOperation(value = "商家接手订单", httpMethod = "POST")
    @PostMapping("android/acceptorder")
    public ResponseObject android_findDjs(HttpServletRequest request, HttpServletResponse response, String orderId) {
            return new ResponseObject(false, "请联系qq471594060");
    }


    /////////////////////////////////////////////////////////////android/////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////android/////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////android/////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////android/////////////////////////////////////////////////////////////////

}
