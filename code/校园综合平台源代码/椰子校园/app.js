App({

  globalData: {
    IP:'https://www.xxxxx.cn'
  },

  onLaunch: function() {
    var that = this;
  },

  login: function(){
    var that = this;
    if (wx.getStorageSync("schoolId")){
      wx.showLoading({
        title: '加载中',
        mask: true
      })
      wx.login({
        success: function (res) {
          wx.request({
            url: that.globalData.IP + '/ops/user/wx/login', //仅为示例，并非真实的接口地址
            data: {
              code: res.code,
              schoolId: wx.getStorageSync("schoolId")
            },
            dataType: 'json',
            method: 'GET',
            header: {
              'content-type': 'application/x-www-form-urlencoded',
            },
            success(res) {
              //成功获取用户信息
              wx.hideLoading()
              console.log(res)
              // res.data.params.user.nickName = decodeURI(res.data.params.user.nickName)
              wx.setStorageSync('token', res.data.params.token);
              wx.setStorageSync('user', res.data.params.user);
            },
            fail: function () {
              wx.hideLoading()
              wx.showToast({
                title: '网络状态实在太差，请退出小程序重新试试',
                icon: 'none',
                duration: 2000,
                mask: true,
              })
            }
          })
        }
      })
    }
  },
  //请求获取接口的封装
  post: function(url, data, callback) {
    var that = this;
    wx.request({
      url: this.globalData.IP + url,
      data: data,
      dataType: 'json',
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'token': wx.getStorageSync("token")
      },
      success: function(res) {
        if (!res.data.code){
          if (res.data.msg == "1100") {
            wx.login({
              success: function (res) {
                wx.request({
                  url: that.globalData.IP  + '/ops/user/wx/login', //仅为示例，并非真实的接口地址
                  data: {
                    code: res.code,
                    schoolId: wx.getStorageSync("schoolId")
                  },
                  dataType: 'json',
                  method: 'GET',
                  header: {
                    'content-type': 'application/x-www-form-urlencoded',
                  },
                  success(res) {
                    //成功获取用户信息
                    wx.setStorageSync('token', res.data.params.token);
                    wx.request({
                      url: that.globalData.IP  + url,
                      data: data,
                      dataType: 'json',
                      method: 'POST',
                      header: {
                        'content-type': 'application/x-www-form-urlencoded',
                        'token': res.data.params.token
                      },
                      success: function (res) {
                        callback(res);
                      },
                      fail: function () {
                        wx.showToast({
                          title: '网络状态实在太差，请退出小程序重新试试',
                          icon: 'none',
                          duration: 2000,
                          mask: true,
                        })
                      }
                    })
                  }
                })
              }
            })
          } else {
            callback(res);
          }
        }else{
          callback(res);
        }
      },
      fail: function() {
        wx.hideLoading()
        wx.showToast({
          title: '网络状态实在太差，请退出小程序重新试试',
          icon: 'none',
          duration: 2000,
          mask: true,
        })
      }
    })
  },
  //请求获取接口的封装
  get: function (url, data, callback) {
    var that = this;
    wx.request({
      url: this.globalData.IP + url,
      data: data,
      dataType: 'json',
      method: 'GET',
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'token': wx.getStorageSync("token")
      },
      success: function (res) {
        if (!res.data.code) {
          if (res.data.msg == "1100") {
            wx.login({
              success: function (res) {
                wx.request({
                  url: that.globalData.IP  + '/ops/user/wx/login', //仅为示例，并非真实的接口地址
                  data: {
                    code: res.code,
                    schoolId: wx.getStorageSync("schoolId")
                  },
                  dataType: 'json',
                  method: 'GET',
                  header: {
                    'content-type': 'application/x-www-form-urlencoded',
                  },
                  success(res) {
                    //成功获取用户信息
                    wx.setStorageSync('token', res.data.params.token);
                    wx.request({
                      url: that.globalData.IP + url,
                      data: data,
                      dataType: 'json',
                      method: 'GET',
                      header: {
                        'content-type': 'application/x-www-form-urlencoded',
                        'token': res.data.params.token
                      },
                      success: function (res) {
                        callback(res);
                      },
                      fail: function () {
                        wx.showToast({
                          title: '网络状态实在太差，请退出小程序重新试试',
                          icon: 'none',
                          duration: 2000,
                          mask: true,
                        })
                      }
                    })
                  }
                })
              }
            })
          } else {
            callback(res);
          }
        } else {
          callback(res);
        }
      },
      fail: function () {
        wx.hideLoading()
        wx.showToast({
          title: '网络状态实在太差，请退出小程序重新试试',
          icon: 'none',
          duration: 2000,
          mask: true,
        })
      }
    })
  },
  //获取屏幕实际大小
  getWindow: function(that) {
    wx.getSystemInfo({
      success: function(res) {
        that.setData({
          innerHeight: res.windowHeight,
          innerWidth: res.windowWidth
        })
      },
    })
  },
})