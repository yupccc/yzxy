var app = getApp();
var that

Page({

  data: {
    phone:''
  },

  onLoad: function (options) {
    this.findSchool()
  },

  findSchool: function () {
    var that = this
    app.post('/ops/school/find', {
      page: 1,
      size: 1,
      orderBy: "sort desc",
      queryType: "school",
      id: wx.getStorageSync("schoolId")
    }, function (res) {
      if (res.data.code) {
        that.setData({
          phone: res.data.params.list[0].phone
        })
      }else{
        wx.showToast({
          title: res.data.msg,
          icon: 'none',
          duration: 2000,
          mask: true,
        })
      }
    })
  },

  tellPhone:function(){
    wx.makePhoneCall({
      phoneNumber: this.data.phone
    })
  },

  //转发后显示的内容
  onShareAppMessage: function () {
    return {
      title: '快来和我一起享校园品质生活吧！！',
      path: '/pages/index/index',
    }
  },
})