var app = getApp();
var that;

Page({

  data: {
    list: [
      {
        title: '个人钱包',
        src: '/images/vipCard.png',
        bind: 'navToVipCard'
      }, {
      title: '收货地址',
      src: '/images/location2.png',
      bind: 'navToLocation'
      },{
      title: '积分商城',
      src: '/images/jiFen.png',
      bind: 'navToIntegral'
      }]
  },

  onLoad: function (options) {
    that = this
  },

  navToLocation: function () {
    wx.navigateTo({
      url: '/pages/mine/location/location',
    })
  },

  navToRunner: function () {
    wx.navigateToMiniProgram({
      appId: 'wxcd8d8750ddbc5ce4',
      path: '/pages/index/index',
      envVersion: 'release',
      success(res) {
   
      }
    })
  },

  navToVipCard: function () {
    wx.navigateTo({
      url: '/pages/mine/payment/payment',
    })
  },

  navToIntegral: function () {
    wx.navigateTo({
      url: '/pages/mine/integral/integral',
    })
  },

  navToAbout: function () {
    wx.navigateTo({
      url: '/pages/mine/about/about',
    })
  },

  navToHelp: function () {
    wx.navigateTo({
      url: '/pages/mine/help/help',
    })
  },

  navToWelcome: function () {
    wx.navigateTo({
      url: '/pages/welcome/welcome?chongxuan=' + 1 + '&menu=' + 0,
    })
  },

  //转发后显示的内容
  onShareAppMessage: function () {
    return {
      title: '快来和我一起享校园品质生活吧！！',
      path: '/pages/index/index',
    }
  },
})