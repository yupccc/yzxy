var app = getApp();
var that;

Page({

  data: {
    notices: [],
    load: true
  },

  showLoading() {
    this.setData({
      load: true
    })
  },
  hideLoading() {
    this.setData({
      load: false
    })
  },

  onLoad: function (options) {
    that = this;
  },

  //请求广播
  findNotice:function(){
    that.showLoading()
    app.post('/ops/notice/find', {
      schoolId: wx.getStorageSync("schoolId")
    }, function (res) {
      if (res.data.code) {
        for (var i in res.data.params.list) {
          res.data.params.list[i].createTime = res.data.params.list[i].createTime.substring(0, 19);
        }
        that.setData({
          notices: res.data.params.list
        })
        that.hideLoading()
      }else{
        that.hideLoading()
        wx.showToast({
          title: res.data.msg,
          icon: 'none',
          duration: 2000,
          mask: true,
        })
      }
    })
  },

  onShow: function () {
    this.findNotice()
  },

  //转发后显示的内容
  onShareAppMessage: function () {
    return {
      title: '快来和我一起享校园品质生活吧！！',
      path: '/pages/index/index',
    }
  },
})