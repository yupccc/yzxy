var app = getApp();
var that;

Page({

  data: {
    schoolName: "暂未选择所在学校",
    swiperCurrent: 0,
    swiperCurrent2: 0,
    slider: [],
    slider2: [
      { mediaUrl: '/images/bunner3.png', sunwouId: 0, bindtap: 'toCoupon' },
      { mediaUrl: '/images/bunner.png', sunwouId: 1, bindtap: 'toVipCard' },
      { mediaUrl: '/images/bunner2.png', sunwouId: 2, bindtap: 'toIntegral' }
    ],
    notice: [],
    autoplay: false,
    circular: true,
    previousmargin: '15px',
    nextmargin: '15px',
    load: false,
    sign: false,
    tis: false,
    isFollow: false,
    score: 0,
    day:0,
    nextscore: 0,
    handPage: 1,
    handSize: 6,
    handTotal: 0,
    source: 0,
    money: 0,
    foodCoupon:0,
    pageLayout:1 ,
    showdetail: false,
  },

  showLoading() {
    that.setData({
      load: true
    })
  },
  hideLoading() {
    that.setData({
      load: false
    })
  },

  onLoad: function (options) {
    that = this;
    app.getWindow(this);
  },

  onShow: function () {
    that.findUserMoney()
    that.toWelcome();
    this.setData({
      autoplay: true
    })
    wx.setNavigationBarTitle({
      title: wx.getStorageSync("schoolName"),
    })
    if (!wx.getStorageSync("tis") && wx.getStorageSync("schoolId")) {
      wx.setStorageSync('tis', true);
      that.setData({
        tis: true,
      })
    }
  },

  //强制到欢迎页，请求顶部学校节日信息
  toWelcome: function () {
    if (!wx.getStorageSync("schoolId")) {
      wx.navigateTo({
        url: '/pages/welcome/welcome?menu=' + 0,
      })
    } else {
      if (!wx.getStorageSync("chongxuang2")) {
        if (that.data.slider.length == 0) {
          that.swiper();
        }
      } else if (wx.getStorageSync("chongxuang2")) {
        that.swiper();
        wx.removeStorageSync("chongxuang2");
      }
    }
  },

  findUserMoney: function () {
    if (wx.getStorageSync("schoolId")){
      app.get('/ops/user/wx/get/bell', {
        openId: wx.getStorageSync("user").openId,
      }, function (res) {
        if (res.data.code) {
          that.setData({
            source: res.data.params.bell.source ? res.data.params.bell.source.toFixed(1) : 0,
            money: res.data.params.bell.money ? res.data.params.bell.money.toFixed(1) : 0,
          })
          wx.setStorageSync('userMoney', res.data.params.bell.money ? res.data.params.bell.money.toFixed(1) : 0);
        }else{
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000,
            mask: true,
          })
        }
      })
    }
  },
  
  //打开签到弹窗
  showSign: function () {
    if (!wx.getStorageSync("phone")){
      wx.showModal({
        title: '提示',
        content: '请先前往个人中心绑定手机号',
        showCancel: true,
        confirmColor: '#3797ee',
        confirmText: '前往绑定',
        success: function (res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/mine/payment/payment',
            })
          }
        }
      })
    }else{
      wx.showLoading({
        title: '加载中',
        mask: true
      })
      app.post('/ops/sign/add', {
        openId: wx.getStorageSync("user").openId,
      }, function (res) {
        if (res.data.code) {
          if (res.data.msg == "今日已签过") {
            wx.showToast({
              title: '今日已签过',
              duration: 2000,
              image: '/images/tanHao.png',
              mask: true
            })
          } else if (res.data.msg == "成功获得3积分") {
            that.findUserMoney()
            that.setData({
              sign: true,
              score: 3,
              day: 1,
              nextscore: 6
            })
            wx.hideLoading()
          } else if (res.data.msg == "成功获得6积分") {
            that.findUserMoney()
            that.setData({
              sign: true,
              score: 6,
              day: 2,
              nextscore: 9
            })
            wx.hideLoading()
          } else if (res.data.msg == "成功获得9积分") {
            that.findUserMoney()
            that.setData({
              sign: true,
              score: 9,
              day: 3,
              nextscore: 12
            })
            wx.hideLoading()
          } else if (res.data.msg == "成功获得12积分") {
            that.findUserMoney()
            that.setData({
              sign: true,
              score: 12,
              day: 4,
              nextscore: 15
            })
            wx.hideLoading()
          } else if (res.data.msg == "成功获得15积分") {
            that.findUserMoney()
            that.setData({
              sign: true,
              score: 15,
              day: 5,
              nextscore: 18
            })
            wx.hideLoading()
          } else if (res.data.msg == "成功获得18积分") {
            that.findUserMoney()
            that.setData({
              sign: true,
              score: 18,
              day: 6,
              nextscore: 21
            })
            wx.hideLoading()
          } else if (res.data.msg == "成功获得21积分") {
            that.findUserMoney()
            that.setData({
              sign: true,
              score: 21,
              day: 7,
              nextscore: 3
            })
            wx.hideLoading()
          }
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000,
            mask: true,
          })
        }
      })
    }
  },
  close: function () {
    that.setData({
      tis: false,
      sign: false,
      isFollow: false,
      showdetail: false
    })
  },

  close2:function(){
    that.setData({
      showdetail: false
    })
  },

  //请求轮播图
  swiper: function () {
    app.get('/ops/slide/find', {
      schoolId: wx.getStorageSync("schoolId"),
    }, function (res) {
      if (res.data.code) {
        that.setData({
          slider: res.data.params.list
        })
        that.findSchool()
        that.getMallList(1);
        that.getArticle();
      }else{
        wx.showToast({
          title: res.data.msg,
          icon: 'none',
          duration: 2000,
          mask: true,
        })
      }
    })
  },

  //查询学校
  findSchool: function () {
    app.post('/ops/school/find', {
      page: 1,
      size: 1,
      orderBy: "sort desc",
      queryType: "school",
      id: wx.getStorageSync("schoolId")
    }, function (res) {
      if (res.data.code) {
        wx.setStorageSync('school', res.data.params.list[0]);
        that.setData({
          takout: res.data.params.list[0].enableTakeout,
          pageLayout: res.data.params.list[0].pageLayout
        })
      }else{
        wx.showToast({
          title: res.data.msg,
          icon: 'none',
          duration: 2000,
          mask: true,
        })
      }
    })
  },

  // 轮播图切换动作函数
  swiperChange: function (e) {
    this.setData({
      swiperCurrent: e.detail.current
    })
  },
  swiperChange2: function (e) {
    this.setData({
      swiperCurrent2: e.detail.current
    })
  },
  //滑动图片切换  
  chuangEvent: function (e) {
    this.setData({
      swiperCurrent: e.currentTarget.id
    })
  },

  //请求热门推荐
  getMallList: function (page) {
    app.get('/ops/secondhand/find', { page: page, size: 6, isShow: 1}, function (res) {
      if (res.data.code) {
        for (var i = 0; i < res.data.params.list.length; i++) {
          res.data.params.list[i].image = res.data.params.list[i].image.split(",");
        }
        that.hideLoading
        that.setData({
          goodList: res.data.params.list,
          handTotal: res.data.params.total,
          handPage: that.data.handPage,
          handSize: that.data.handSize
        })
      }else{
        that.hideLoading
        wx.showToast({
          title: res.data.msg,
          icon: 'none',
          duration: 2000,
          mask: true,
        })
      }
    })
  },

  //请求公告
  getArticle: function () {
    app.get('/ops/notice/find', {
      schoolId: wx.getStorageSync("schoolId")
    }, function (res) {
      if (res.data.code) {
        that.setData({
          notice: res.data.params.list
        })
      }else{
        wx.showToast({
          title: res.data.msg,
          icon: 'none',
          duration: 2000,
          mask: true,
        })
      }
    })
  },

  // 跳转去公告
  toArticle: function () {
    wx.navigateTo({
      url: '/pages/index/notice/notice',
    })
  },

  // 跳转去跑腿
  navToRun: function () {
    wx.navigateTo({
      url: '/pages/index/run/run',
    })
  },
  // 跳转去合伙人
  navToPartner: function (e) {
    wx.navigateTo({
      url: '/pages/index/partner/partner',
    })
  },
  // 跳转二手
  navToMall: function (e) {
    wx.navigateTo({
      url: '/pages/index/mall/mall',
    })
  },
  // 跳转积分
  navToIntegral: function () {
    wx.navigateTo({
      url: '/pages/mine/integral/integral',
    })
  },
  // 刷新二手
  refresh: function () {
    var that = this
    if (parseInt((that.data.handTotal - that.data.handSize) / 6) >= 1) {
      that.data.handPage++;
      that.data.handSize += 6;
      that.getMallList(that.data.handPage);
    } else {
      that.data.handPage = 1;
      that.data.handSize = 6;
      that.getMallList(1);
    }
  },
  // 跳转去二手物品详情
  navToDetails: function (e) {
    wx.navigateTo({
      url: '/pages/index/mall/details/details?data=' + JSON.stringify(that.data.goodList[e.currentTarget.dataset.index]),
    })
  },

  //转发后显示的内容
  onShareAppMessage: function () {
    return {
      title: '快来和我一起享校园品质生活吧！！',
      path: '/pages/index/index',
    }
  },
  toVipCard: function () {
    wx.navigateTo({
      url: '/pages/mine/payment/payment',
    })
  },

  toIntegral: function () {
    wx.navigateTo({
      url: '/pages/mine/integral/integral',
    })
  },

  onHide: function () {
    that.setData({
      autoplay: false
    })
  },

})