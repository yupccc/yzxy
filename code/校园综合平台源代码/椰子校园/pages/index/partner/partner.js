var app = getApp();
var that;

Page({

  data: {
    isFollow: false
  },

  apply: function(){
    this.setData({
      isFollow: true
    })
  },

  close: function(){
    this.setData({
      isFollow: false
    })
  },

  onLoad: function (options) {
    that = this
  },

  //转发后显示的内容
  onShareAppMessage: function () {
    return {
      title: '快来和我一起享校园品质生活吧！！',
      path: '/pages/index/index',
    }
  },
})